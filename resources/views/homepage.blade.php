@extends('layouts.laylaravel')
<title>Home - Scrumptious</title>
@section('content')

    <h2>Stop waiting.<br>Start building.<br></h2><a class="btn btn-outline-warning btn-xl js-scroll-trigger" role="button" href="/project">Let's Get scrumming!</a></div>
@endsection
