@extends('layouts.layout')
<title>Home - Scrumptious</title>
@section('content')
    <div class="container-fluid">
        <h3 class="text-dark mb-4">Profile</h3>
        <div class="row mb-3">
            <div class="col-lg-4">
                <div class="card mb-3">
                    <div class="card-body text-center shadow">
                        <img class="rounded-circle mb-3 mt-4"
                             src="profiles/{{ Auth::user()->username }}/pf/pf.jpeg"
                             width="180" height="180">
                        <div class="mb-3">
                            <form action="/pf" method="post" enctype="multipart/form-data">
                                @csrf
                                <input class="btn btn-primary btn-sm" type="file" name="fileToUpload" id="fileToUpload">
                                <input class="btn btn-primary btn-sm" type="submit" value="Upload Image" name="submit">
                                <div>
                                    @if ($errors->has('fileToUpload'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('fileToUpload') }}</strong>
                                    </span>
                                        @endif
                                    {{ $er ?? '' }}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <h6 class="text-primary font-weight-bold m-0">Projects</h6>
                    </div>
                    <div class="card-body">
                        <h4 class="small font-weight-bold">Server migration<span class="float-right">20%</span></h4>
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-danger" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 20%;"><span class="sr-only">20%</span></div>
                        </div>
                        <h4 class="small font-weight-bold">Sales tracking<span class="float-right">40%</span></h4>
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-warning" aria-valuenow="40" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 40%;"><span class="sr-only">40%</span></div>
                        </div>
                        <h4 class="small font-weight-bold">Customer Database<span class="float-right">60%</span></h4>
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-primary" aria-valuenow="60" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 60%;"><span class="sr-only">60%</span></div>
                        </div>
                        <h4 class="small font-weight-bold">Payout Details<span class="float-right">80%</span></h4>
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-info" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 80%;"><span class="sr-only">80%</span></div>
                        </div>
                        <h4 class="small font-weight-bold">Account setup<span class="float-right">Complete!</span></h4>
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-success" aria-valuenow="100" aria-valuemin="0"
                                 aria-valuemax="100" style="width: 100%;"><span class="sr-only">100%</span></div>
                        </div>
                    </div>
                </div>



            </div>
            <div class="col-lg-8">
                <div class="row mb-3 d-none">
                    <div class="col">
                        <div class="card text-white bg-primary shadow">
                            <div class="card-body">
                                <div class="row mb-2">
                                    <div class="col">
                                        <p class="m-0">Peformance</p>
                                        <p class="m-0"><strong>65.2%</strong></p>
                                    </div>
                                    <div class="col-auto"><i class="fas fa-rocket fa-2x"></i></div>
                                </div>
                                <p class="text-white-50 small m-0"><i class="fas fa-arrow-up"></i>&nbsp;5% since last
                                    month</p>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card text-white bg-success shadow">
                            <div class="card-body">
                                <div class="row mb-2">
                                    <div class="col">
                                        <p class="m-0">Peformance</p>
                                        <p class="m-0"><strong>65.2%</strong></p>
                                    </div>
                                    <div class="col-auto"><i class="fas fa-rocket fa-2x"></i></div>
                                </div>
                                <p class="text-white-50 small m-0"><i class="fas fa-arrow-up"></i>&nbsp;5% since last
                                    month</p>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col">
                        <div class="card shadow mb-3">
                            <div class="card-header py-3">
                                <p class="text-primary m-0 font-weight-bold">User Settings</p>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="profile/{{ Auth::user()->id }}">
                                    @method('PATCH')
                                    @csrf
                                    @foreach($user as $u)
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-group"><label for="username"><strong>Username</strong></label><input
                                                        class="form-control" type="text" value="{{ $u->username }}"
                                                        name="username"></div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group"><label for="email"><strong>Email
                                                            Address</strong></label><input class="form-control"
                                                                                           type="email"
                                                                                           value="{{ $u->email }}"
                                                                                           name="email"></div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col">
                                                <div class="form-group"><label for="name"><strong>First
                                                            Name</strong></label><input class="form-control" type="text"
                                                                                        value="{{ $u->name }}"
                                                                                        name="name"></div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group"><label for="lastname"><strong>Last Name</strong></label><input
                                                        class="form-control" type="text" value="{{ $u->lastname }}"
                                                        name="lastname"></div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-primary btn-sm" type="submit">Save Settings</button>
                                        </div>
                                    @endforeach
                                </form>
                                @include('/errors/errors')
                            </div>
                        </div>


                        <div class="card shadow">
                            <div class="card-header py-3">
                                <p class="text-primary m-0 font-weight-bold">Change Password</p>
                            </div>
                            <div class="card-body">
                                <form class="form-horizontal" method="POST" action="{{ route('changePassword') }}">
                                    {{ csrf_field() }}

                                    <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                        <label for="new-password" class="col-md-4 control-label">Current
                                            Password</label>

                                        <div class="col-md-6">
                                            <input id="current-password" type="password" class="form-control"
                                                   name="current-password" required>

                                            @if ($errors->has('current-password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('current-password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                        <label for="new-password" class="col-md-4 control-label">New Password</label>

                                        <div class="col-md-6">
                                            <input id="new-password" type="password" class="form-control"
                                                   name="new-password" required>

                                            @if ($errors->has('new-password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('new-password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="new-password-confirm" class="col-md-4 control-label">Confirm New
                                            Password</label>

                                        <div class="col-md-6">
                                            <input id="new-password-confirm" type="password" class="form-control"
                                                   name="new-password_confirmation" required>
                                        </div>
                                    </div>
                                    @if($pass ?? '' !== '')
                                        <div class="btn btn-primary">
                                            {{ $pass ?? '' }}
                                        </div>
                                        <br/><br/>
                                        @else
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Change Password
                                                </button>
                                            </div>
                                        </div>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
{{--        <div class="card shadow mb-5">--}}
{{--            <div class="card-header py-3">--}}
{{--                <p class="text-primary m-0 font-weight-bold">Forum Settings</p>--}}
{{--            </div>--}}
{{--            <div class="card-body">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-6">--}}
{{--                        <form>--}}
{{--                            <div class="form-group"><label--}}
{{--                                    for="signature"><strong>Signature</strong><br></label><textarea class="form-control"--}}
{{--                                                                                                    rows="4"--}}
{{--                                                                                                    name="signature"></textarea>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <div class="custom-control custom-switch"><input class="custom-control-input"--}}
{{--                                                                                 type="checkbox" id="formCheck-1"><label--}}
{{--                                        class="custom-control-label" for="formCheck-1"><strong>Notify me about new--}}
{{--                                            replies</strong></label></div>--}}
{{--                            </div>--}}
{{--                            <div class="form-group">--}}
{{--                                <button class="btn btn-primary btn-sm" type="submit">Save Settings</button>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>



@endsection
