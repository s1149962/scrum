@extends('/layouts.scrum')
<title>Projects - Scrumptious</title>
@section('content')
    <body class="text-center">
    {{--TO DO--}}
    <div class="card shadow mb-5" style="width: 365px; display: inline-block; height: 700px; overflow-y: scroll;">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">To Do</h6>
        </div>
        @foreach($scrum->sortbydesc('priority') as $rule)
            @if($rule->progress == 'todo')
                <div style="height: 8%; background-color: whitesmoke;">
                    <form style="display: inline;" action="/scrumboard/{{$rule->id}}" id="drop" method="POST">
                        @csrf
                        @method('PATCH')
                        <select onchange="this.form.submit('drop')"
                                style="background-color:@if($rule->priority == '1') green; @elseif($rule->priority == '2')#ffc500; @else red; @endif color: white; font-size: 10px; width: 80px;"
                                name="priority" class="font-weight-bold m-0 btn-sm prior float-left">
                            @if($rule->priority == '1')
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '2')
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '3')
                                <option class="high" value="3">High</option>
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                            @else
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @endif
                        </select>
                    </form>
                    @if($rule->progress !== 'done')
                        <form style="display: inline-block; height: 10px;" class="float-right" action="/scrumboard"
                              method="POST">
                            @csrf
                            <button name="+" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm"
                                    style="color: white; display: inline; font-size: 12px;">->
                            </button>
                        </form>
                    @endif
                    @if($rule->progress !== 'todo')
                        <form style="display: inline-block; height: 10px;" class="float-right"
                              action="/scrumboard/{{ $id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button name="-" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm float-left"
                                    style="color: white; display: inline; font-size: 12px;"><-
                            </button>
                        </form>
                    @endif
                    <?php
                    $open = $rule->id;
                    ?>
                    <div onclick="myFunction({{$open}})">
                        <span class="text-primary font-weight-bold m-0 btn-sm" style="font-size: 12px;">S.Points:{{$rule->storypoints}} | B.Value:{{$rule->value}}</span><br/>
                        <a class="text-primary font-weight-bold m-0 btn-sm float-left"
                           style="font-size: 14px;">{{$rule->title}}</a>
                    </div>
                    <script>
                        function myFunction($open) {
                            var x = document.getElementById($open);
                            if (x.style.display === "none") {
                                x.style.display = "block";
                            } else {
                                x.style.display = "none";
                            }
                        }
                    </script>
                </div>
                <div  class="card-body" style="margin-top: -1.1em; margin-bottom: -0.2em;">
                    <div style="display:none;" id="{{$open}}" class="text-primary font-weight-bold m-0 btn-sm"
                         type="text">{{ $rule->info }} <br/><br/></div>
                    <hr onclick="myFunction({{$open}})" style="margin-bottom: -0.7em; margin-top: -0.5em; border-width: 5px;">
                </div>

            @endif
        @endforeach
    </div>
    {{--END TO DO--}}
    {{--DOING--}}
    <div class="card shadow mb-5" style="width: 365px; display: inline-block; height: 700px; overflow-y: scroll;">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">Doing</h6>
        </div>
        @foreach($scrum->sortbydesc('priority') as $rule)
            @if($rule->progress == 'doing')
                <div style="height: 8%; background-color: whitesmoke;">
                    <form style="display: inline;" action="/scrumboard/{{$rule->id}}" id="drop" method="POST">
                        @csrf
                        @method('PATCH')
                        <select onchange="this.form.submit('drop')"
                                style="background-color:@if($rule->priority == '1') green; @elseif($rule->priority == '2')#ffc500; @else red; @endif color: white; font-size: 10px; width: 80px;"
                                name="priority" class="font-weight-bold m-0 btn-sm prior float-left">
                            @if($rule->priority == '1')
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '2')
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '3')
                                <option class="high" value="3">High</option>
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                            @else
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @endif
                        </select>
                    </form>
                    @if($rule->progress !== 'done')
                        <form style="display: inline-block; height: 10px;" class="float-right" action="/scrumboard"
                              method="POST">
                            @csrf
                            <button name="+" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm"
                                    style="color: white; display: inline; font-size: 12px;">->
                            </button>
                        </form>
                    @endif
                    @if($rule->progress !== 'todo')
                        <form style="display: inline-block; height: 10px;" class="float-right"
                              action="/scrumboard/{{ $id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button name="-" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm float-left"
                                    style="color: white; display: inline; font-size: 12px;"><-
                            </button>
                        </form>
                    @endif
                    <?php
                    $open = $rule->id;
                    ?>
                    <div onclick="myFunction({{$open}})">
                        <span class="text-primary font-weight-bold m-0 btn-sm" style="font-size: 12px;">S.Points:{{$rule->storypoints}} | B.Value:{{$rule->value}}</span><br/>
                        <a class="text-primary font-weight-bold m-0 btn-sm float-left"
                           style="font-size: 14px;">{{$rule->title}}</a>
                    </div>
                    <script>
                        function myFunction($open) {
                            var x = document.getElementById($open);
                            if (x.style.display === "none") {
                                x.style.display = "block";
                            } else {
                                x.style.display = "none";
                            }
                        }
                    </script>
                </div>
                <div  class="card-body" style="margin-top: -1.1em; margin-bottom: -0.2em;">
                    <div style="display:none;" id="{{$open}}" class="text-primary font-weight-bold m-0 btn-sm"
                         type="text">{{ $rule->info }} <br/><br/></div>
                    <hr onclick="myFunction({{$open}})" style="margin-bottom: -0.7em; margin-top: -0.5em; border-width: 5px;">
                </div>

            @endif
        @endforeach
    </div>
    {{--END DOING--}}
    {{--CHECK--}}
    <div class="card shadow mb-5" style="width: 365px; display: inline-block; height: 700px; overflow-y: scroll;">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">Check</h6>
        </div>
        @foreach($scrum->sortbydesc('priority') as $rule)
            @if($rule->progress == 'check')
                <div style="height: 8%; background-color: whitesmoke;">
                    <form style="display: inline;" action="/scrumboard/{{$rule->id}}" id="drop" method="POST">
                        @csrf
                        @method('PATCH')
                        <select onchange="this.form.submit('drop')"
                                style="background-color:@if($rule->priority == '1') green; @elseif($rule->priority == '2')#ffc500; @else red; @endif color: white; font-size: 10px; width: 80px;"
                                name="priority" class="font-weight-bold m-0 btn-sm prior float-left">
                            @if($rule->priority == '1')
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '2')
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '3')
                                <option class="high" value="3">High</option>
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                            @else
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @endif
                        </select>
                    </form>
                    @if($rule->progress !== 'done')
                        <form style="display: inline-block; height: 10px;" class="float-right" action="/scrumboard"
                              method="POST">
                            @csrf
                            <button name="+" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm"
                                    style="color: white; display: inline; font-size: 12px;">->
                            </button>
                        </form>
                    @endif
                    @if($rule->progress !== 'todo')
                        <form style="display: inline-block; height: 10px;" class="float-right"
                              action="/scrumboard/{{ $id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button name="-" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm float-left"
                                    style="color: white; display: inline; font-size: 12px;"><-
                            </button>
                        </form>
                    @endif
                    <?php
                    $open = $rule->id;
                    ?>
                    <div onclick="myFunction({{$open}})">
                        <span class="text-primary font-weight-bold m-0 btn-sm" style="font-size: 12px;">S.Points:{{$rule->storypoints}} | B.Value:{{$rule->value}}</span><br/>
                        <a class="text-primary font-weight-bold m-0 btn-sm float-left"
                           style="font-size: 14px;">{{$rule->title}}</a>
                    </div>
                    <script>
                        function myFunction($open) {
                            var x = document.getElementById($open);
                            if (x.style.display === "none") {
                                x.style.display = "block";
                            } else {
                                x.style.display = "none";
                            }
                        }
                    </script>
                </div>
                <div  class="card-body" style="margin-top: -1.1em; margin-bottom: -0.2em;">
                    <div style="display:none;" id="{{$open}}" class="text-primary font-weight-bold m-0 btn-sm"
                         type="text">{{ $rule->info }} <br/><br/></div>
                    <hr onclick="myFunction({{$open}})" style="margin-bottom: -0.7em; margin-top: -0.5em; border-width: 5px;">
                </div>

            @endif
        @endforeach
    </div>
    {{--END CHECK--}}
    {{--DONE--}}
    <div class="card shadow mb-5" style="width: 365px; display: inline-block; height: 700px; overflow-y: scroll;">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">Done</h6>
        </div>
        @foreach($scrum->sortbydesc('priority') as $rule)
            @if($rule->progress == 'done')
                <div style="height: 8%; background-color: whitesmoke;">
                    <form style="display: inline;" action="/scrumboard/{{$rule->id}}" id="drop" method="POST">
                        @csrf
                        @method('PATCH')
                        <select onchange="this.form.submit('drop')"
                                style="background-color:@if($rule->priority == '1') green; @elseif($rule->priority == '2')#ffc500; @else red; @endif color: white; font-size: 10px; width: 80px;"
                                name="priority" class="font-weight-bold m-0 btn-sm prior float-left">
                            @if($rule->priority == '1')
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '2')
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                                <option class="high" value="3">High</option>
                            @elseif($rule->priority == '3')
                                <option class="high" value="3">High</option>
                                <option class="med" value="2">Medium</option>
                                <option class="low" value="1">Low</option>
                            @else
                                <option class="low" value="1">Low</option>
                                <option class="med" value="2">Medium</option>
                                <option class="high" value="3">High</option>
                            @endif
                        </select>
                    </form>
                    @if($rule->progress !== 'done')
                        <form style="display: inline-block; height: 10px;" class="float-right" action="/scrumboard"
                              method="POST">
                            @csrf
                            <button name="+" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm"
                                    style="color: white; display: inline; font-size: 12px;">->
                            </button>
                        </form>
                    @endif
                    @if($rule->progress !== 'todo')
                        <form style="display: inline-block; height: 10px;" class="float-right"
                              action="/scrumboard/{{ $id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button name="-" value="{{ $rule->id }}"
                                    class="btn-outline-warning btn-primary btn-sm float-left"
                                    style="color: white; display: inline; font-size: 12px;"><-
                            </button>
                        </form>
                    @endif
                    <?php
                    $open = $rule->id;
                    ?>
                    <div onclick="myFunction({{$open}})">
                        <span class="text-primary font-weight-bold m-0 btn-sm" style="font-size: 12px;">S.Points:{{$rule->storypoints}} | B.Value:{{$rule->value}}</span><br/>
                        <a class="text-primary font-weight-bold m-0 btn-sm float-left"
                           style="font-size: 14px;">{{$rule->title}}</a>
                    </div>
                    <script>
                        function myFunction($open) {
                            var x = document.getElementById($open);
                            if (x.style.display === "none") {
                                x.style.display = "block";
                            } else {
                                x.style.display = "none";
                            }
                        }
                    </script>
                </div>
                <div  class="card-body" style="margin-top: -1.1em; margin-bottom: -0.2em;">
                    <div style="display:none;" id="{{$open}}" class="text-primary font-weight-bold m-0 btn-sm"
                         type="text">{{ $rule->info }} <br/><br/></div>
                    <hr onclick="myFunction({{$open}})" style="margin-bottom: -0.7em; margin-top: -0.5em; border-width: 5px;">
                </div>

            @endif
        @endforeach
    </div>
    {{--END DONE--}}


    <a href="/project/{{$id}}" class="row justify-content-center font-weight-bold">Back</a><br/><br/>
    </body>
@endsection
