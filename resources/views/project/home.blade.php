@extends('layouts.layout')
<title>Projects - Scrumptious</title>
@section('content')


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">Projects</h6>
        </div>
        <div class="card-body">
            @if (empty($projects))
                <div class="text-center font-weight-bold">You have no projects!</div> <br/>

            @else
                @foreach($projects as $project)
                    @php
                        if (trim(\App\Scrum::all()->where('Scrum_project_id', $project->id)->whereIn('progress', ['PBL','todo','doing','check','done','completed'])->pluck('storypoints'), '[]') == ''){
                        $percentage = 0;
                        } else{
                           $notdone = \App\Scrum::all()->where('Scrum_project_id', $project->id)->whereIn('progress', ['PBL','todo','doing','check','done','completed'])->pluck('storypoints')->toArray();
                                            $done = \App\Scrum::all()->where('Scrum_project_id', $project->id)->where('progress', 'completed')->pluck('storypoints')->toArray();
                        $percentage = round(abs(100 / array_sum($notdone) * array_sum($done)));
                        }
                    @endphp
                    <a href="/project/{{ $project->id }}">
                        <h4 class="small font-weight-bold">{{ $project->projectname }} <span class="float-right">{{$percentage}}%</span>
                        </h4>

                    @if($percentage <= 25)
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-danger" aria-valuenow="{{$percentage}}"
                                 aria-valuemin="0"
                                 aria-valuemax="100"
                                 style="width: {{ $percentage}}%;"><span
                                    class="sr-only">{{$percentage}}</span></div>
                        </div>
                    @elseif($percentage <= 45)
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-warning" aria-valuenow="{{$percentage}}"
                                 aria-valuemin="0"
                                 aria-valuemax="100"
                                 style="width: {{ $percentage}}%;"><span
                                    class="sr-only">{{$percentage}}</span></div>
                        </div>
                    @elseif($percentage <= 70)
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-primary" aria-valuenow="{{$percentage }}"
                                 aria-valuemin="0"
                                 aria-valuemax="100"
                                 style="width:{{$percentage}}%;"><span
                                    class="sr-only">{{ $percentage }}</span></div>
                        </div>
                    @elseif($percentage <= 99)
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-info" aria-valuenow="{{$percentage }}"
                                 aria-valuemin="0"
                                 aria-valuemax="100"
                                 style="width:{{$percentage }}%;"><span
                                    class="sr-only">{{ $percentage }}</span></div>
                        </div>
                    @elseif($percentage == 100)
                        <div class="progress progress-sm mb-3">
                            <div class="progress-bar bg-success" aria-valuenow="100"
                                 aria-valuemin="0"
                                 aria-valuemax="100"
                                 style="width: 100%;"><span class="sr-only">Completed</span>
                            </div>
                        </div>
                    @endif
                    </a>
                @endforeach
            @endif
            <form method="POST" action="/project" class="text-center">
                @csrf
                <h2 class="text-primary font-weight-bold m-0">Want to create a Scrum Project?</h2>
                <h2 class="text-primary font-weight-bold m-0"><input
                        class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                        type="text" name="projectname" style="width:250px;font-size:12pt;"
                        placeholder="enter the Scrum project name">
                    <button class="btn-primary  btn-sm" type="submit">Create Project</button>
                </h2>
                <br/>
                <div style="background-color: red;color: white; font-size: 20px;">@include('/errors/errors')</div>
            </form>
        </div>
    </div>



@endsection
