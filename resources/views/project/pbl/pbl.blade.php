@extends('/layouts.scrum')
<title>Projects - Scrumptious</title>
@section('content')
    <div class="card shadow mb-4" style="height: 560px; width: 100%; overflow-y: scroll;">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">Product Backlog</h6>
        </div>
        <table class="ta" style="width:100%; font-size:18px;">
            <tr>
                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                    <th>-Delete</th>
                @endif
                <th>+SBL</th>
                <th>title</th>
                <th>Information</th>
                <th>B.value</th>
                <th>S.points</th>
                <th>Priority</th>
                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                    <th>change</th>
                @endif
            </tr>
            @foreach($scrum->sortbydesc('priority') as $rule)

                <tr>
                    @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                        <form style="display: inline" action="/pbl/{{ $id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <td>
                                <button name="del" value="{{ $rule->id }}" class="btn-outline-danger btn-primary btn-sm"
                                        style="color: white">Delete
                                </button>
                            </td>
                        </form>
                    @endif
                    <td>
                        <form style="display: inline" action="/pbl/create" method="POST">
                            @csrf
                            @method('HEAD')
                            <button class="btn-outline-success btn-primary  btn-sm" style="color: white"
                                    value="{{ $rule->id }}" name="SBL">Move
                            </button>
                        </form>
                    </td>

                    @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                        <form style="display: inline" action="/pbl/{{ $rule->id }}" method="POST">
                            @endif
                            @csrf
                            @method('PATCH')
                            <td>
                                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                                    <input class="text-primary font-weight-bold m-0 btn-sm"
                                           style="width: 200px;" type="text" name="title" value="{{ $rule->title }}">
                                @else
                                    <div class="text-primary font-weight-bold m-0 btn-sm">{{ $rule->title }}</div>
                                @endif
                            </td>
                            <td>
                                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                                    <input class="text-primary font-weight-bold m-0 btn-sm"
                                           style="width: 920px;" type="text" name="info" value="{{ $rule->info }}">
                                @else
                                    <div class="text-primary font-weight-bold m-0 btn-sm">{{ $rule->info }}</div>
                                @endif

                            </td>

                            <td> @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                                    <input class="text-primary font-weight-bold m-0 btn-sm"
                                           style="width: 80px;" type="number" name="value" value="{{ $rule->value }}">
                                @else
                                    <div class="text-primary font-weight-bold m-0 btn-sm">{{ $rule->value }}</div>
                                @endif
                            </td>

                            <td>
                                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                                    <input class="text-primary font-weight-bold m-0 btn-sm"
                                           style="width: 80px;" type="number" name="storypoints"
                                           value="{{ $rule->storypoints }}">
                                @else
                                    <div class="text-primary font-weight-bold m-0 btn-sm">{{ $rule->storypoints }}</div>
                                @endif

                            </td>
                            <td>
                                @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                                    <select
                                        style="background-color:@if($rule->priority == '1') green; @elseif($rule->priority == '2')#ffc500; @else red; @endif color: white;"
                                        name="priority" class="font-weight-bold m-0 btn-sm prior">
                                        @if($rule->priority == '1')
                                            <option class="low" value="1">Low</option>
                                            <option class="med" value="2">Medium</option>
                                            <option class="high" value="3">High</option>
                                        @elseif($rule->priority == '2')
                                            <option class="med" value="2">Medium</option>
                                            <option class="low" value="1">Low</option>
                                            <option class="high" value="3">High</option>
                                        @elseif($rule->priority == '3')
                                            <option class="high" value="3">High</option>
                                            <option class="med" value="2">Medium</option>
                                            <option class="low" value="1">Low</option>
                                        @else
                                            <option class="low" value="1">Low</option>
                                            <option class="med" value="2">Medium</option>
                                            <option class="high" value="3">High</option>
                                        @endif
                                    </select>
                                @else
                                    <form style="display: inline;" action="/scrumboard/{{$rule->id}}" id="drop"
                                          method="POST">
                                        @csrf
                                        @method('PATCH')
                                        <select onchange="this.form.submit('drop')"
                                                style="background-color:@if($rule->priority == '1') green; @elseif($rule->priority == '2')#ffc500; @else red; @endif color: white;"
                                                name="priority" class="font-weight-bold m-0 btn-sm prior float-left">
                                            @if($rule->priority == '1')
                                                <option class="low" value="1">Low</option>
                                                <option class="med" value="2">Medium</option>
                                                <option class="high" value="3">High</option>
                                            @elseif($rule->priority == '2')
                                                <option class="med" value="2">Medium</option>
                                                <option class="low" value="1">Low</option>
                                                <option class="high" value="3">High</option>
                                            @elseif($rule->priority == '3')
                                                <option class="high" value="3">High</option>
                                                <option class="med" value="2">Medium</option>
                                                <option class="low" value="1">Low</option>
                                            @else
                                                <option class="low" value="1">Low</option>
                                                <option class="med" value="2">Medium</option>
                                                <option class="high" value="3">High</option>
                                            @endif
                                        </select>
                                    </form>
                                @endif
                            </td>
                            @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                                <td>
                                    <button class="btn-outline-warning btn-primary  btn-sm" style="color: white">Change
                                    </button>
                                </td>
                            @endif
                            @if(\App\Project::find($id)->owner_id == Auth::user()->id)
                        </form>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
    <div class="card shadow mb-4">
        <form method="POST" action="/pbl/{{$id}}/edit" class="text-center">
            @csrf
            @method('HEAD')
            <h2 class="text-primary font-weight-bold m-0">Create a Backlog item</h2>
            <h2 class="text-primary font-weight-bold m-0">

                <input
                    class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                    type="text" name="title" style="width:300px;font-size:12pt;"
                    value="{{  old('title', '') }}"
                    placeholder="title of Backlog Item">

                <input
                    class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                    type="text" name="item" style="width:600px;font-size:12pt;"
                    value="{{  old('item', '') }}"
                    placeholder="As a < type of user >, I want < goal > so that < reason >">
                <input
                    class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                    type="number" name="storypoints" style="width:100px;font-size:12pt;"
                    value="{{  old('storypoints', '') }}"
                    placeholder="S.points">
                <input
                    class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                    type="number" name="value" style="width:100px;font-size:12pt;"
                    value="{{  old('value', '') }}"
                    placeholder="B.value">
                <select
                    style="color: white;"
                    name="priority" class="btn-primary  btn-sm">
                    <option class="low" value="1">Low</option>
                    <option class="med" value="2">Medium</option>
                    <option class="high" value="3">High</option>
                </select>

                <button class="btn-primary  btn-sm" type="submit">Add Item</button>

            </h2>
            <br/>
            <div style="background-color: red;color: white; font-size: 20px;">@include('/errors/errors')</div>
            <a href="/project/{{$id}}" class=" font-weight-bold">Back</a><br/>

            <br/>
        </form>
    </div>
@endsection

