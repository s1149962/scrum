@extends('/layouts.scrum')
<title>Projects - Scrumptious</title>
@section('content')
    @if(empty($scrum))
        <div class="card shadow mb-4 text-center font-weight-bold m-0" style="height: 50px; width: 100%;">
            You have not completed any Backlog Items!
        </div>
    @else
        <div class="card shadow mb-4" style="height: 560px; width: 100%; overflow-y: scroll;">
            <div class="card-header py-3">
                <h6 class="text-primary font-weight-bold m-0">Sprint Backlog</h6>
            </div>
            <table class="ta" style="width:100%; font-size:18px;">
                <tr>
                    <th>-PBL</th>
                    <th>title</th>
                    <th>Information</th>
                    <th>B.value</th>
                    <th>S.points</th>
                    <th>Priority</th>
                    <th>Progress</th>
                </tr>
                @foreach($scrum->sortbydesc('priority') as $rule)
                    <td>
                        <form style="display: inline;" action="/sbl/{{ $id }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button name="PBL" value="{{ $rule->id }}" class="btn-outline-warning btn-primary btn-sm"
                                    style="color: white">Move
                            </button>
                        </form>
                    </td>


                    <td>
                        <div class="text-primary font-weight-bold m-0 btn-sm">
                            {{ $rule->title }}
                        </div>
                    </td>
                    <td>
                        <div class="text-primary font-weight-bold m-0 btn-sm">
                            {{ $rule->info }}
                        </div>
                    </td>

                    <td>
                        <div class="text-primary font-weight-bold m-0 btn-sm">
                            {{$rule->value}}
                        </div>
                    </td>

                    <td>
                        <div class="text-primary font-weight-bold m-0 btn-sm">
                            {{$rule->storypoints}}
                        </div>
                    </td>
                    <td>
                        <form style="display: inline;" action="/scrumboard/{{$rule->id}}" id="drop" method="POST">
                            @csrf
                            @method('PATCH')
                            <select onchange="this.form.submit('drop')"
                                    style="background-color:@if($rule->priority == '1') green; @elseif($rule->priority == '2')#ffc500; @else red; @endif color: white;"
                                    name="priority" class="font-weight-bold m-0 btn-sm prior float-left">
                                @if($rule->priority == '1')
                                    <option class="low" value="1">Low</option>
                                    <option class="med" value="2">Medium</option>
                                    <option class="high" value="3">High</option>
                                @elseif($rule->priority == '2')
                                    <option class="med" value="2">Medium</option>
                                    <option class="low" value="1">Low</option>
                                    <option class="high" value="3">High</option>
                                @elseif($rule->priority == '3')
                                    <option class="high" value="3">High</option>
                                    <option class="med" value="2">Medium</option>
                                    <option class="low" value="1">Low</option>
                                @else
                                    <option class="low" value="1">Low</option>
                                    <option class="med" value="2">Medium</option>
                                    <option class="high" value="3">High</option>
                                @endif
                            </select>
                        </form>
                    </td>
                    <td>
                        <div class="text-primary font-weight-bold m-0 btn-sm">
                            <button
                                style="color: white; width: 90px; background-color: @if($rule->progress == 'done')green;@else red; @endif">{{$rule->progress}}</button>
                        </div>
                    </td>
                    </tr>
                @endforeach
            </table>
        </div>
        @if(\App\Project::find($id)->owner_id == Auth::user()->id)
        <span class="card shadow mb-4" style="display: inline-block; width: 100%">
    <button style="color: white; display: inline-block; width: 70%;" class=" btn-primary btn-sm">The total Storypoints of this sprint are: {{array_sum($scrum->pluck('storypoints')->toArray())}}</button>
        <form style="display: inline;" method="POST" action="/sbl/{{$id}}">
            @csrf
            @method('PATCH')
        <button style="display: inline-block; width: 29%; color: white;" class="btn-danger btn-sm">END SPRINT</button>
            </form>
    </span>
        @else
            <span class="card shadow mb-4" style="display: inline-block; width: 100%">
    <button style="color: white; display: inline-block; width: 100%;" class=" btn-primary btn-sm">The total Storypoints of this sprint are: {{array_sum($scrum->pluck('storypoints')->toArray())}}</button>
            </span>
            @endif
        <a href="/project/{{$id}}" class="row justify-content-center font-weight-bold">Back</a><br/><br/>
    @endif
@endsection
