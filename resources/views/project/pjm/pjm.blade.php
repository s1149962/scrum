@extends('/layouts.scrum')
<title>Projects - Scrumptious</title>
@section('content')


    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="text-primary font-weight-bold m-0">Project Managment</h6>
        </div>
        <div class="card-body">

            <form method="POST" action="/pjm/{{$id}}" class="text-center">
                @csrf
                @method('PATCH')
                @if(empty($add))
                <h2 class="text-primary font-weight-bold m-0">Want to be a teamplayer?</h2>
                <h2 class="text-primary font-weight-bold m-0"><input
                        class="btn-outline-light text-primary font-weight-bold m-0 btn-sm"
                        type="text" name="add" style="width:250px;font-size:12pt;"
                        placeholder="enter the username or email">
                    <button class="btn-primary  btn-sm" type="submit">Add person</button>
                    @elseif(!empty($add))
                    <br/><br/>
                    <div style="background-color: @if($add == 'user was added!') green; @else  red; @endif color: white; font-size: 20px;">{{$add ?? ''}}</div>
                        @endif
                </h2>
                <div style="background-color: red;color: white; font-size: 20px;">@include('/errors/errors')</div>
            </form>
        </div>
        <div class="card-body">
            <div class="table-responsive table mt-2" id="dataTable" role="grid" aria-describedby="dataTable_info">
                <table class="table dataTable my-0" id="dataTable">
                    <thead>
                    <tr>
                        <td><strong>Username</strong></td>
                        <td><strong>E-mail</strong></td>
                        <td><strong>Name</strong></td>
                        <td><strong>Last Name</strong></td>
                        <td><strong>Make Owner</strong></td>
                        <td><strong>REMOVE</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($team->users as $per)
                    <tr>
                        <td><img class="rounded-circle mr-2" width="30" height="30" src="/profiles/{{ $per->username }}/pf/pf.jpeg">{{$per->username}}</td>
                        <td>{{$per->email}}</td>
                        <td>{{$per->name}}</td>
                        <td>{{$per->lastname}}</td>
                        <form action="/pjm/{{$id}}/edit">
                        <td> <button class=" btn-warning  btn-sm small font-weight-bold" name="make" value="{{$per->id}}">Make Owner</button><br></td>
                        </form>
                        <form method="POST" action="/pjm/{{$id}}">
                            @csrf
                            @method('DELETE')
                        <td> <button class=" btn-danger  btn-sm small font-weight-bold" name="remove" value="{{$per->id}}">Remove</button></td>
                        </form>
                    </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <td><strong>Username</strong></td>
                        <td><strong>E-mail</strong></td>
                        <td><strong>Name</strong></td>
                        <td><strong>Last Name</strong></td>
                        <td><strong>Make Owner</strong></td>
                        <td><strong>REMOVE</strong></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <a href="/project/{{$id}}" class="text-center font-weight-bold">Back</a><br/>
    </div>


@endsection
