<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Login - Scrumptious</title>
    <meta name="description" content="The number one scrum utilities! (don't look that up)">
    <link rel="stylesheet" href="index/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli">
    <link rel="stylesheet" href="index/fonts/font-awesome.min.css">
</head>

<body id="page-top">
<nav class="navbar navbar-light navbar-expand-lg fixed-top" id="mainNav">
    <div class="container"><a class="navbar-brand js-scroll-trigger" href="/home">Scrumptious</a><button data-toggle="collapse" data-target="#navbarResponsive" class="navbar-toggler float-right" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><i class="fa fa-bars"></i></button>
        <div
            class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="/contact">Contact</a></li>
                @guest
                    <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="/login">login</a></li>
                    @if (Route::has('register'))
                        <li class="nav-item" role="presentation"><a class="nav-link js-scroll-trigger" href="/register">register</a></li>
                    @endif
                @else

                    <li class="nav-item" role="presentation" aria-labelledby="navbarDropdown">
                        <a class="nav-link js-scroll-trigger" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
            </ul>
            @endguest
        </div>
    </div>
</nav>
<style>
    .bg{
        height: 100%;
        background-image: linear-gradient(180deg, #2af598 0%, #009efd 100%);



        /* Center and scale the image nicely */
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>
<section class="cta bg">
    <div class="cta-content">
        <div class="container">
            @yield('content')
        </div>
    </div>
    <div class="overlay"></div>
</section>
<script src="index/js/jquery.min.js"></script>
<script src="/assets/js/scroll.js"></script>
<script src="index/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="index/js/new-age.js"></script>
</body>

</html>
