<!DOCTYPE html>
<html >

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>{{ config('', 'Scrumptious') }}</title>
    <meta name="description" content="The number one site gor scrum utilities! (dont look that up)">
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/bootstrap/css/board.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="/assets/fonts/fontawesome-all.min.css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body id="page-top">
<div id="wrapper">

    <div class="d-flex flex-column" id="content-wrapper">
        <div id="content">
            <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                <a style="color: #4e73df" class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="/home">
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-laugh-wink"></i></div>
                    <div class="sidebar-brand-text mx-3"><span>scrumptious</span></div>
                </a>
                <div class="container-fluid">
                    <button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i
                            class="fas fa-bars"></i></button>

                    <ul class="nav navbar-nav flex-nowrap ml-auto">
                        <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link"
                                                                            data-toggle="dropdown" aria-expanded="false"
                                                                            href="#"><i class="fas fa-search"></i></a>
                            <div class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" role="menu"
                                 aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto navbar-search w-100">
                                    <div class="input-group"><input class="bg-light form-control border-0 small"
                                                                    type="text" placeholder="Search for ...">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary py-0" type="button"><i
                                                    class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <div class="shadow dropdown-list dropdown-menu dropdown-menu-right"
                             aria-labelledby="alertsDropdown"></div>
                        </li>
                        <div class="d-none d-sm-block topbar-divider"></div>
                        <li class="nav-item dropdown no-arrow" role="presentation">
                            <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link"
                                                                       data-toggle="dropdown" aria-expanded="false"
                                                                       href="#"><span
                                        class="d-none d-lg-inline mr-2 text-gray-600 small">{{ Auth::user()->username }}</span><img
                                        class="border rounded-circle img-profile"
                                        src="/profiles/{{ Auth::user()->username }}/pf/pf.jpeg"></a>
                                <div
                                    class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu"><a
                                        class="dropdown-item" role="presentation" href="/profile"><i
                                            class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;How to Scrum</a><a
                                        class="dropdown-item" role="presentation" href="/profile"><i
                                            class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;profile</a>
                                    <a
                                        class="dropdown-item" role="presentation" href="/project"><i
                                            class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Projects</a>

                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" role="presentation" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a>
                                </div>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>


                            </div>
                        </li>
                    </ul>
                </div>
            </nav>


            <div class="container-fluid">
                @yield('content')

            </div>
        </div>

        <style>
            .footer {
                position: fixed;
                text-align: center;
                bottom: -10px;
                width: 100%;
            }
        </style>
        <br/>

        <footer class="bg-white sticky-footer page-down footer">
            <div class="container my-auto">
                <div class="text-center my-auto copyright"><span>Copyright © scrumptious 2019</span></div>
            </div>
        </footer>
    </div>
</div>
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/js/scroll.js"></script>
<script src="/assets/js/chart.min.js"></script>
<script src="/assets/js/bs-init.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
<script src="/assets/js/theme.js"></script>
</body>

</html>
