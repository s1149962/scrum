<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'projectname'
    ];

// What user owns this project and is therefore its administrator
    public function owner() {
        return $this->belongsTo('App\User', 'owner_id');
    }

    // What other users can participate in this project
    public function users() {
        return $this->belongsToMany('App\User', 'groups', 'project_id', 'user_id');
    }

}
