<?php
// profile
namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Hash;


class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::all()->where('id', Auth::user()->id);
        return view('/profile/home', compact('user'));
    }


    public function update(User $user)
    {
        request()->validate([
            'name' => ['required'],
            'lastname' => 'required'
        ]);
        if (request('username') !== Auth::user()->username) {
            request()->validate([
                'username' => ['required', 'unique:users']
            ]);
        }
        if (request('email') !== Auth::user()->email) {
            request()->validate([
                'email' => ['required', 'email', 'unique:users']
            ]);
        }
        if (!file_exists(__DIR__ . '/../../../public/profiles/' . Auth::user()->username)) {
            mkdir(__DIR__ . '/../../../public/profiles/' . Auth::user()->username);
            mkdir(__DIR__ . '/../../../public/profiles/' . Auth::user()->username . '/pf');
            copy(__DIR__ . '/../../../public/copies/' . '/pf.jpeg', __DIR__ . '/../../../public/profiles/' . Auth::user()->username . '/pf/pf.jpeg');
        }
        rename(__DIR__ . '/../../../public/profiles/' . Auth::user()->username, __DIR__ . '/../../../public/profiles/' . request('username'));
        Auth::user()->update([
            Auth::user()->name = request('name'),
            Auth::user()->lastname = request('lastname'),
            Auth::user()->email = request('email'),
            Auth::user()->username = request('username')
        ]);
        return redirect('/profile');
    }


    public function pf(User $user)
    {
        request()->validate([
            'fileToUpload' => ['required']
        ]);
        $target_dir = __DIR__ . '/../../../public/profiles/' . Auth::user()->username . '/pf/';
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        }
        if ($check == false) {
            $er = "File is not an image.";
        } elseif ($_FILES["fileToUpload"]["size"] > 500000) {
            $er = "Sorry, your file is too large.";
        } elseif ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
            $er = "Sorry, only JPG, JPEG, PNG  files are allowed.";
        } elseif ($check !== false) {
            $er = "File is an image - " . $check["mime"] . ".";
            if (!file_exists(__DIR__ . '/../../../public/profiles/' . Auth::user()->username)) {
                mkdir(__DIR__ . '/../../../public/profiles/' . Auth::user()->username);
                mkdir(__DIR__ . '/../../../public/profiles/' . Auth::user()->username . '/pf');
                copy(__DIR__ . '/../../../public/copies/' . '/pf.jpeg', __DIR__ . '/../../../public/profiles/' . Auth::user()->username . '/pf/pf.jpeg');
            }
            $_FILES["fileToUpload"]["name"] = 'pf.jpeg';
            $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                $er = "The file has been uploaded.";
            } else {
                $er = "Sorry, there was an error uploading your file.";
            }
        }
        if ($er == '') {
            $er = 'something went wrong';
        }
        $user = User::all()->where('id', Auth::user()->id);
        header("refresh:3; url=/profile");
        return view('/profile/home', compact('user', 'er'));
    }


    public function changePassword(Request $request)
    {

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            $pass = "Your current password does not matches with the password you provided. Please try again.";
            $user = User::all()->where('id', Auth::user()->id);
            header("refresh:3; url=/profile");
            return view('/profile/home', compact('user', 'pass'));
        }

        if (strcmp($request->get('current-password'), $request->get('new-password')) == 0) {
            //Current password and new password are same
            $pass = "New Password cannot be same as your current password. Please choose a different password.";
            $user = User::all()->where('id', Auth::user()->id);
            header("refresh:3; url=/profile");
            return view('/profile/home', compact('user', 'pass'));
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        $pass = 'you succesfully changed your password';
        $user = User::all()->where('id', Auth::user()->id);
        header("refresh:3; url=/profile");
        return view('/profile/home', compact('user', 'pass'));

    }

}
