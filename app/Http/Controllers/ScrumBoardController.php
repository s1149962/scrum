<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scrum;
class ScrumBoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $back =  Scrum::find(request('+'));
        if ($back->progress == 'todo') {
            Scrum::find(request('+'))->update([
                'progress' => 'doing',
            ]);
        }
        elseif ($back->progress == 'doing') {
            Scrum::find(request('+'))->update([
                'progress' => 'check',
            ]);
        }
        elseif ($back->progress == 'check') {
            Scrum::find(request('+'))->update([
                'progress' => 'done',
            ]);
        }
        return back();
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $points = Scrum::all()->where('Scrum_project_id', $id)->whereIn('progress', ['todo', 'doing', 'check', 'done'])->pluck('storypoints')->toArray();
        $scrum = Scrum::all()->where('Scrum_project_id', $id)->whereIn('progress', ['todo', 'doing', 'check', 'done']);
        return view('/project/scrumboard/scrumboard', compact('id', 'scrum', 'points'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        request()->validate([
            'rule' => ['required', 'max:150'],
        ]);
        $scrum = new Scrum;
        $scrum->info = request('rule');
        $scrum->Scrum_project_id = $id;
        $scrum->progress = 'SBL';
        $scrum->save();

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'priority' => ['required'],
        ]);
        Scrum::find($id)->update([
            "priority" => request('priority'),
        ]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Scrum $scrum)
    {
        $back =  Scrum::find(request('-'));
        if ($back->progress == 'doing') {
            Scrum::find(request('-'))->update([
                'progress' => 'todo',
            ]);
        }
        elseif ($back->progress == 'check') {
            Scrum::find(request('-'))->update([
                'progress' => 'doing',
                ]);
        }
        elseif ($back->progress == 'done') {
            Scrum::find(request('-'))->update([
                'progress' => 'check',
            ]);
        }
        return back();
    }
}
