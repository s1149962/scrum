<?php

namespace App\Http\Controllers;

use App\Dod;
use Illuminate\Http\Request;
use App\Scrum;
class DodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $scrum = Scrum::all()->where('Scrum_project_id', $id)->where('progress', 'DOD');
        return view('/project/dod/dod', compact('id', 'scrum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        request()->validate([
            'rule' => ['required', 'max:150'],
            'title' => ['required', 'max:20'],
        ]);
        $scrum = new Scrum;
        $scrum->title = request('title');
        $scrum->info = request('rule');
        $scrum->Scrum_project_id = $id;
        $scrum->progress = 'DOD';
        $scrum->save();

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Scrum::where('id', request('del'))->delete();
        return back();
    }
}
