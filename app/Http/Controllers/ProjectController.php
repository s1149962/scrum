<?php
// project
namespace App\Http\Controllers;

use App\Project;
use App\Scrum;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Group;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Project $project
     * @param Group $group
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, Group $group, User $user, Scrum $scrum)
    {
        $projects = $user->find(Auth::User()->id)->projects;
//        $group = Group::all()->where('user_id', Auth::User()->id);
//        foreach ($group as $g) {
//            $projects[$g->id] = Project::find($g->project_id);;
//        }

        if (empty($projects)) {
            return view('/project/home');
        } else {
            return view('/project/home', compact('projects'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public
    function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public
    function store(Request $request, Project $project)
    {
        request()->validate([
            'projectname' => ['required'],
        ]);
        $new = new Project;
        $new->owner_id = Auth::user()->id;
        $new->projectname = request('projectname');
        $new->save();

        $new2 = new Group;
        $new2->user_id = Auth::user()->id;
        $new2->project_id = trim(Project::all()->where('projectname', request('projectname'))->pluck('id')->last(), '[]');
        $new2->save();
    return back();
}

    /**
     * Display the specified resource.
     *
     * @param \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public
    function show($id)
    {
//    $u = Project::find($id)->users;


//    $group = Group::all()->where('project_id', $id);
//    foreach ($group as $g) {
//        $u[$g->user_id] = User::find($g->user_id);
//    }

        $project = Project::find($id);
        return view('/project/show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public
    function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public
    function update(Project $project)
    {
        request()->validate([
            'projectname' => ['required'],
        ]);

        Project::find($project->id)->update([
            'projectname' => request('projectname')
        ]);


        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        Project::where('id', $id)->delete();
        return redirect('/project');
    }
}
