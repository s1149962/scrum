<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// for now a redirect bc only using /  will cause issues on adsd server
Route::get('/', function () {
    return view('/index');
});
// HOMEPAGE
Route::get('/home', function () {
    return view('/homepage');
});
// AUTH ROUTE
Auth::routes();

// PROFILE ROUTES
Route::post('/pf', 'ProfileController@pf');
Route::resource('/profile', 'ProfileController');
Route::post('/changePassword','ProfileController@changePassword')->name('changePassword');

//  PROJECT ROUTES
Route::resource('/project', 'ProjectController');

// PJM ROUTES
Route::resource('/pjm', 'PJMController');
Route::get('/pjm/{pjm}', 'PJMController@index');

// DOD ROUTES
Route::resource('/dod', 'DodController');

// PBL ROUTES
Route::resource('/pbl', 'PBLController');

// SBL ROUTES
Route::resource('/sbl', 'SBLController');

// scrumboard ROUTES
Route::resource('/scrumboard', 'ScrumBoardController');

// completed ROUTES
Route::resource('/completed', 'CompletedController');






